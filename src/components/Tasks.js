import React from 'react';
import Task from './Task'

const Tasks = ({ tasks, onDelete, onToggle}) => {

  return (
    // setTasks([{id: 4, text: 'from setTask'}],
    <>
      {tasks.map((task) => (
        <Task key={task.id} task={task} onDelete={onDelete} onToggle={onToggle} />
      ))}
    </>
  )
}

export default Tasks
