import PropTypes from 'prop-types'
import Button from './Button'

const Header = ({ title, onAdd, showAdd }) => {
  const btnHeaderClick = () => {
    console.log('trigger from header');
  }
  return (
    <header className='header'>
      <h1>{ title }</h1>
      <Button color='blue' text='Add' onClick={btnHeaderClick}/>
      <Button text={showAdd ? 'Hide' : 'Add'} onClick={onAdd} color={showAdd && 'red'}/>
      {/* <Button color='red' text='Test red' /> */}
      
    </header>
  )
}

Header.defaultProps = {
  title: 'Task Tracker'
}

Header.propTypes = {
  title: PropTypes.string.isRequired,
}

const headingStyle = {
  color: 'green', backgroundColor: 'red'
}

export default Header
